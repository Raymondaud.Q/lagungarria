# Lagungarria

C'est une application pour poster des annonces géolocalisées.  
Docker, Django, Expo, React-Native, Jest, PLSQL & GitLab CI   

# Instructions 

* [Front (React-Native + JEST + EXPO)](https://gitlab.com/Raymondaud.Q/lagungarria/-/tree/master/Lagungarria-Client)
* [Back (Docker + Django + PLSQL)](https://gitlab.com/Raymondaud.Q/lagungarria/-/tree/master/Lagungarria-Server)

# Preview

![Architecture](https://gitlab.com/Raymondaud.Q/lagungarria/-/raw/master/external_resources/Lagungarria-Final.png)
![Screenshots](https://gitlab.com/Raymondaud.Q/lagungarria/-/raw/master/external_resources/Screenss.png)
