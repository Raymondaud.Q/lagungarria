# Installation 

I've followed : https://docs.docker.com/samples/django/
and https://www.django-rest-framework.org/tutorial/1-serialization/

* apt install docker.io
* apt install docker-compose


# Run

## Start

* sudo docker-compose build
* sudo docker-compose up
* sudo docker exec -it django python manage.py migrate --run-syncdb
* sudo docker exec django python manage.py test lagungarriapi.tests_views
* sudo docker exec -it django python manage.py createsuperuser --email admin@example.com --username admin

## Test

* Go to http://localhost:8000/dj-rest-auth/login/ login and go to http://localhost:8000/snippets/ then post the following lines :
	* `{
        "nickname": "nick",
        "textContent":  "Saluce",
        "latitude": 40.00002,
        "longitude": 6.01242
        }` 
    * **TESTS** with `apt install httpie` 
    * **POST** test : 
        * http -f POST http://localhost:8000/snippets/ Authorization:"Bearer `your_token`" nickname=admin textContent=`msg` latitude=`lat` longitude=`long` 
        * http -f POST http://localhost:8000/snippets/ Authorization:"Bearer `your_token`" nickname=admin textContent=`msg` latitude=`lat` longitude=`long` image@`/path/to/image`

    * **DELETE** test : http -f DELETE http://localhost:8000/snippets/id Authorization:"Bearer `your_token`"

    
* **Access snippet API** : http://localhost:8000/snippets.api
	* **JSON List Snippets** : http://localhost:8000/snippets.json
	* **List Snippets** : http://localhost:8000/snippets.api
	* **Snippet Details** : http://localhost:8000/snippets/1.api
	* **JSON Snippet Details** : http://localhost:8000/snippets/1.json  
    * **Snippet around you** : http://localhost:8000/snippetbyloc/?latitude=44.008644&longitude=4.872589&perimeter=2
        * **GET** Test :  http 'http://localhost:8000/snippetbyloc/?latitude=`latitude`&longitude=`longitude`&perimeter=`perimeter in km`'
    * **Snippet[OFFSSET:LIMIT]** : http://localhost:8000/snippetbetween/?start=1&end=5
        * **GET** Test : http 'http://localhost:8000/snippetbetween/?start=1&end=5'
  
* **Access Admin Page** : http://localhost:8000/admin/
  
* **Registration Page** : http://localhost:8000/dj-rest-auth/registration/
    * **POST** Test : http -f POST http://localhost:8000/dj-rest-auth/registration/ username=`username` password1=`password` password2=`password`  email=`emailaddress`
  
* **Access JWT Page** : http://localhost:8000/api/token/
    * **POST** Test: http -f POST http://localhost:8000/api/token/ username=`username` password=`password`
  
* **Login PAGE** : http://localhost:8000/dj-rest-auth/login/
    * **POST** Test : http -f POST http://localhost:8000/dj-rest-auth/login/  username=`username` password=`password`
  
* **Password Change page** : http://localhost:8000/dj-rest-auth/password/change/
    * **POST** Test : http -f http://localhost:8000/dj-rest-auth/password/change/ Authorization:"Bearer `your_token`" new_password1=`newpassword` new_password2=`newpassword` old_password=`oldpassword`
  
* **Password Reset Page** : http://localhost:8000/dj-rest-auth/password/reset/
    * **POST** Test : http -f POST http://localhost:8000/dj-rest-auth/password/reset/ email=`emailaddress`
    * then : http -f POST http://localhost:8000/dj-rest-auth/password/reset/confirm/ uid=`uid` token=`token` new_password1=`newpassword` new_password2=`newpassword`
  
Read https://dj-rest-auth.readthedocs.io/en/latest/index.html and https://www.django-rest-framework.org/api-guide/authentication/ for more infos
## Restart after Stop

* sudo docker-compose up

## Stop

* sudo docker-compose down


## Add a new app in /app

> Execute django-admin cmd in /app folder : `sudo docker-compose run web django-admin <startproject/startapp/...>`

1. **CREATE THE APP MODULE** : sudo docker-compose run web django-admin startapp <module_name>
2. **MIGRATE THE APP MODEL** : sudo docker exec -it <container_id> python manage.py makemigrations
3. **APPLY MIGRATIONS** : sudo docker exec -it <container_id> python manage.py migrate


## Access Postgres
* sudo docker exec -it <container_id> psql -h localhost -p 5432 -U postgres -W

## Give permission
* sudo chown -R $USER:$USER .

## Uninstall

* sudo docker image prune -a
* sudo docker container prune
