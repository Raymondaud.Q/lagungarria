from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings

class Snippet(models.Model):

    nickname = models.CharField(max_length=100)
    textContent = models.TextField()
    latitude = models.DecimalField(max_digits=22, decimal_places=16) 
    longitude = models.DecimalField(max_digits=22, decimal_places=16)
    date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='images/', blank=True)
    
ordering = ['created']

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
    else : 
        from django.contrib.auth.models import User
        for user in User.objects.all():
            Token.objects.get_or_create(user=user)

