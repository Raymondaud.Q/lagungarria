from rest_framework import serializers
from snippets.models import Snippet

class SnippetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Snippet
        fields = [ 'id', 'nickname', 'textContent', 'latitude', 'longitude', 'date', 'image']

