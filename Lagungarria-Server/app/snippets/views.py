from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from snippets.models import Snippet
from snippets.serializers import SnippetSerializer
from geopy import distance
"""
List all code snippets, or create a new snippet.
"""
@api_view(['GET', 'POST'])
def snippet_list(request, format=None):
    if request.method == 'GET':
        snippets = Snippet.objects.all()
        serializer = SnippetSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = SnippetSerializer(data=request.data)
        if serializer.is_valid() and request.data['nickname'] == request.user.username:
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"""
List all code snippets, or create a new snippet.
"""
@api_view(['GET'])
def snippet_between(request, format=None):
    if request.method == 'GET':
        snippets = Snippet.objects.all()[int(request.query_params['start']):int(request.query_params['end'])]
        serializer = SnippetSerializer(snippets, many=True)
        return Response(serializer.data)

"""
List all code snippets, or create a new snippet.
"""
@api_view(['GET'])
def snippet_by_loc(request,  format=None):
    if request.method == 'GET':
        userLoc = ( float(request.query_params['latitude']), float(request.query_params['longitude']) )
        perimeter = float(request.query_params['perimeter'])
        snippets = Snippet.objects.all()
        for snip in snippets :
            snipLoc = ( float(snip.latitude), float(snip.longitude) )
            if distance.distance(userLoc, snipLoc).km > perimeter :
                snippets = snippets.exclude(id=snip.id)
        serializer = SnippetSerializer(snippets, many=True)
        return Response(serializer.data)

"""
Retrieve, update or delete a code snippet.
"""
@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, pk, format=None):
   
    try:
        snippet = Snippet.objects.get(pk=pk)
    except Snippet.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SnippetSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SnippetSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        if request.user.username == snippet.nickname :
            snippet.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else :
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)