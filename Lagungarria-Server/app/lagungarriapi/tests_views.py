from django.test import TestCase
from django.contrib.auth.models import User
from snippets.models import Snippet

class ViewTest(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret1234'
        }
        self.new_credentials = {
            'new_password1': 'new_secret1234',
            'new_password2': 'new_secret1234'
        }
        self.wrong_credentials = {
            'new_password1': 'secret1234',
            'new_password2': 'secret1234'
        }
        self.registration_credentails = {
            'username': 'newaccount123',
            'email': 'acc123@gmail.com',
            'password1': 'okok123okok.',
            'password2': 'okok123okok.'
        }
        self.empty_password = {
            'username': 'pooo111',
            'email': 'pooo111@gmail.com',
            'password1': '',
            'password2': ''
        }
        self.falseCred = {
            'username':'admin',
            'password':'false'
        }
        self.emptyCred = {
            'username':'',
            'password':''
        }
        self.new_snippet = {
            'nickname':'testuser',
            'textContent':'This is a message from testuser !',
            'latitude':'44.008644',
            'longitude':'4.8725',
        }
        User.objects.create_user(**self.credentials)

    def test_home_page(self):
        print('.> test_home_page')
        response = self.client.get('http://localhost:8000/snippets/')
        self.assertEqual(response.status_code, 200)

    def test_login_use_empty_username_password(self):
        print('> test_login_use_empty_username_password')
        response = self.client.post(path='/dj-rest-auth/login/', data=self.emptyCred, follow=True)
        self.assertIn(b'{"password":["This field may not be blank."]}',response.content)

    def test_login_username_or_password_not_correct(self):
        print('> test_login_username_or_password_not_correct')
        response = self.client.post(path='/dj-rest-auth/login/', data=self.falseCred, follow=True)
        self.assertIn(b'{"non_field_errors":["Unable to log in with provided credentials."]}',response.content)

    def test_login(self):
        print('> test_login')
        response = self.client.post(path='/dj-rest-auth/login/', data=self.credentials, follow=True)
        self.assertIn(b'access_token',response.content)

    def test_registration(self):
        print('> test_registration')
        response = self.client.post(path='/dj-rest-auth/registration/', data=self.registration_credentails, follow=True)
        self.assertIn(b'access_token',response.content)

    def test_empty_password(self):
        print('> test_empty_password')
        response = self.client.post(path='/dj-rest-auth/registration/', data=self.empty_password, follow=True)
        self.assertIn(b'{"password1":["This field may not be blank."],"password2":["This field may not be blank."]}',response.content)

    def test_change_pwd(self):
        print('> test_change_password')
        self.client.post('/dj-rest-auth/login/', self.credentials, follow=True)
        response = self.client.post(path='/dj-rest-auth/password/change/', data=self.new_credentials, follow=True)
        self.assertIn(b'{"detail":"New password has been saved."}',response.content)

    def test_change_wrong_pwd(self):
        print('> test_change_wrong_password')
        response = self.client.post(path='/dj-rest-auth/password/change/', data=self.wrong_credentials, follow=True)
        self.assertIn(b'{"detail":"Authentication credentials were not provided."}',response.content)

    def test_post_list_snippet(self):
        print('> test_post_list_snippet')#http://localhost:8000/snippetbyloc/?latitude=44.008644&longitude=4.872589&perimeter=2
        self.client.post('/dj-rest-auth/login/', self.credentials, follow=True)
        self.client.post(path='/snippets/', data=self.new_snippet, follow=True)
        response = self.client.get(path='/snippetbyloc/?latitude=44.008644&longitude=4.872589&perimeter=10', follow=True)
        self.assertIn(b'{"id":1,"nickname":"testuser","textContent":"This is a message from testuser !"',response.content)

    def test_delete_snippet(self):
        print('> test_delete_snippet')#http://localhost:8000/snippetbyloc/?latitude=44.008644&longitude=4.872589&perimeter=2
        self.client.post('/dj-rest-auth/login/', self.credentials, follow=True)
        self.client.delete(path='/snippets/1', follow=True)
        response = self.client.get(path='/snippetbyloc/?latitude=44.008644&longitude=4.872589&perimeter=10', follow=True)
        self.assertEqual([],response.json())