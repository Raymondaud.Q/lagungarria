import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from "expo-status-bar";
import  LoginScreen  from './components/LoginScreen.js';
import  RegisterScreen  from './components/RegisterScreen.js';
import  HomeScreen  from './components/HomeScreen.js';
import  PasswordForgottenScreen  from './components/PasswordForgottenScreen.js';
import  SettingsScreen  from './components/SettingsScreen.js';
import  ChangePwdScreen  from './components/ChangePwdScreen.js';
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";


const Stack = createNativeStackNavigator();
export default () => {  
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} options={{ title: 'Lagungarria - Login' }}/> 
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="Reset" component={PasswordForgottenScreen} />
        <Stack.Screen name="Settings" component={SettingsScreen} />
        <Stack.Screen name="Change Password" component={ChangePwdScreen} />
        <Stack.Screen name="Home" component={HomeScreen} options={{ headerTitle: 'Lagungarria - Home', headerShown: false , headerBackVisible:false }}/> 
      </Stack.Navigator>
    </NavigationContainer>
  );
};

