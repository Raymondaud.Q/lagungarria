module.exports = {
  preset: 'jest-expo/android',
  transform: {
    '^.+\\.js$': 'babel-jest',
  }
};