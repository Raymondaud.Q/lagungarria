import { myAddress } from '../HttpConfig.js';
var FormData = require('form-data');

const testToken = ( token, user, password ) => {
	return fetch("http://"+myAddress()+"/dj-rest-auth/token/verify/", {
	  method: 'POST',
	  headers: {
	    'Content-Type': 'application/json'
	  },
	  body: JSON.stringify({
	    token: token
	  })
	})
}

const login = ( username, password) => {
	return fetch('http://'+myAddress()+'/dj-rest-auth/login/', {
	  method: 'POST',
	  headers: {
	    'Content-Type': 'application/json'
	  },
	  body: JSON.stringify({
	    username: username,
	    password: password
	  })
	});
}

const updatePostList = (token, location, perimeter) => {
	return fetch("http://"+myAddress()+"/snippetbyloc/?latitude="+location.latitude+'&longitude='+location.longitude+'&perimeter='+perimeter, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    }
  })
}

const post = (token,nick,text,location,image) => { 
  const body = new FormData()
  body.append('nickname',nick)
  body.append('textContent', text,)
  body.append('latitude', location.latitude)
  body.append('longitude', location.longitude)
  if ( image ){
    const imageForm = {  uri: image, 
              name:"img-"+nick+"."+image.split('.').pop(),
              type: "image/"+image.split('.').pop() }
    body.append('image',imageForm)
  }
  return fetch("http://"+myAddress()+"/snippets/", {
    method: 'POST',
    headers: {
      'Authorization': 'Bearer '+token
    },
    body
  })
}

const remove = (token,id) => { 
  return fetch("http://"+myAddress()+"/snippets/"+id, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    },
  });
}

const resetPassword = ( email ) => {
	return fetch('http://'+myAddress()+'/dj-rest-auth/password/reset/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email
    })
  })
}

const changePassword = ( token, newPassword ) => {
  return fetch('http://'+myAddress()+'/dj-rest-auth/password/change/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    },
    body: JSON.stringify({
      new_password1:newPassword,
      new_password2:newPassword,
    })
  })
}

const register = (nickname, password, email) => {
	return fetch('http://'+myAddress()+'/dj-rest-auth/registration/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: nickname,
      password1: password,
      password2: password,
      email: email
    })
  })
}

export default { testToken, login, updatePostList, post, remove, resetPassword, register, changePassword  };