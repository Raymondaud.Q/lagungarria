import { NavigationContainer } from '@react-navigation/native';
import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { myAddress } from '../HttpConfig.js';
import APIServices from './APIServices.js';
import Utils from './utils.js';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert
} from "react-native";

const RegisterScreen = ({ navigation }) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nickname, setNickname] = useState("");

  const register = () => {
    APIServices.register(nickname,password,email).then((response) => {
      return response.json()
    }).then((data) => {;
      if ( ! data.user ){
        let msg = ""
        if ( data.username )
          msg += "Nickname : "+ data.username+"\n";
        if ( data.password1 )
          msg += "Password : "+ data.password1+"\n";
        if ( data.email )
          msg += "Email : "+ data.email+"\n";
        Utils.showAlert("Registration FAILED", msg )
      } else {
        let user = JSON.stringify(data.user)
        Utils.save('token', data.access_token );
        Utils.save('profile', user);
        Utils.save('password', password);
        Utils.showAlert("Welcome in Lagungarria !", "Registration & Login complete.");
        navigation.navigate('Home', { JWT: data.access_token, profile: user  } );
      }
    }).catch(err => console.error(err));
  }  

  return (
    <View style={styles.container}>

      <StatusBar style="auto" />

      <Image
        style={styles.image}
        source={require('../assets/logo.jpeg')}
      />

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Your email"
          placeholderTextColor="#fff"
          onChangeText={(email) => setEmail(email)}
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Your nickname"
          placeholderTextColor="#fff"
          onChangeText={(nickname) => setNickname(nickname)}
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Your password."
          placeholderTextColor="#fff"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>

      <TouchableOpacity style={styles.registerBtn}
          onPress={ () => { register() } }>
        <Text style={styles.registerText}>Create</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "#040500",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    marginBottom: 40,
  },

  image: {
    width: "50%",
    height: "20%",
    marginBottom: 100,
  },

  inputView: {
    backgroundColor: "#d93128",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    width:"100%",
    textAlign:"center",
    fontSize: 20,
    color: "#fff"
  },

  registerBtn: {
    width: "60%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#ebedef",
  },

  registerText: {
    color: "#303841",
    fontWeight: "bold",
    fontSize: 20,
  },

});

export default RegisterScreen;