import { NavigationContainer } from '@react-navigation/native';
import React, { useState, useEffect } from "react";
import BigList from "react-native-big-list";
import * as Location from 'expo-location';
import { StatusBar } from "expo-status-bar";
import { myAddress } from '../HttpConfig.js';
import * as SecureStore from 'expo-secure-store';
import Utils from './utils.js';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  ScrollView,
  Dimensions
} from "react-native";

const SettingsScreen = ({ navigation, route }) => {
  const { JWT, profile, location, HomeComponent } = route.params;
  const [oldpwd, setOldPwd] = useState("");
  const [password, setPassword] = useState("");
  const [locNameInput, setLocNameInput] = useState("");
  const [latitudeInput, setLatitudeInput] = useState("");
  const [longitudeInput, setLongitudeInput] = useState("");
  const [locName, setLocName] = useState("");
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [currentLoc, setCurrentLoc] = useState("");
  const [locationList, setLocationList] = useState([]);

  const setGPSLocation = async() => {
    let tmpLocation = {latitude:0, longitude:0}
    await setCurrentLoc(tmpLocation)
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status === 'granted'){
      try {
        let location = await Location.getCurrentPositionAsync( {enableHighAccuracy:false} );
        tmpLocation = {latitude: location.coords.latitude, longitude: location.coords.longitude}
      } catch(e){}
      await setCurrentLoc( tmpLocation );
      setLocName("GPS-"+locationList.length);
      setLatitude(""+tmpLocation.latitude);
      setLongitude(""+tmpLocation.longitude);
      Utils.save("location",'')
    } else {
      alert('Permission to access location was denied');
    }
  }

  const setLocation =  async() => { 
    let tmpLoc = {latitude: latitude,longitude: longitude}
    await setCurrentLoc(tmpLoc)
    Utils.save("location",JSON.stringify(tmpLoc))
  }

  const saveLoc = async() => {
    if ( locName == "" || latitude == "" || longitude == "" )
      return
    if ( ! locationList )
      await setLocationList([])
    const newLocList = [...locationList, ({locName:locName, latitude:latitude, longitude:longitude})]
    await setLocationList(newLocList)
    Utils.save("savedLocations",JSON.stringify(newLocList))
  }

  const deleteLoc =  async(index) => {
    const newLocList = [...locationList]
    newLocList.splice(index,1)
    await setLocationList(newLocList)
    Utils.save("savedLocations",JSON.stringify(newLocList))
  }

  const selectLoc = async(item) => {
    let tmpLocation = {latitude: item.latitude, longitude: item.longitude}
    await setCurrentLoc( tmpLocation );
    setLocName(item.locName);
    setLatitude(tmpLocation.latitude);
    setLongitude(tmpLocation.longitude);
    Utils.save("location",JSON.stringify(tmpLocation))
  }

  const LocListComponent = ({ data }) => {
    const renderItem = ({ item, index }) => (
      <View style={styles.locationBtns}>
        <View style={styles.latLongItem}>
          <TouchableOpacity style={styles.selectLocBtn}
            onPress={async() => { await selectLoc(item) }}>
            { item && <Text style={styles.selectLocText}>{item.locName+" : "+item.latitude+", "+item.longitude+" "}</Text>}
          </TouchableOpacity>
        </View>
        
        <TouchableOpacity style={styles.deleteLocBtn}
          onPress={async () => { await deleteLoc(index) }}>
          <Text style={styles.deleteLocText}>❌</Text>
        </TouchableOpacity>
      </View>
    ); return (
      <View style={{flex:1}}>
        <BigList
          data={data}
          numColumns={1} // Set the number of columns
          renderItem={renderItem}
          itemHeight={50}
          itemWidth={Dimensions.get('window').width}
          headerHeight={0}
          footerHeight={0}
        />
      </View>
    );
  };        

  useEffect(() => {
    (async () => {
      let savedLocations = await SecureStore.getItemAsync('savedLocations');
      if ( savedLocations )
        await setLocationList(JSON.parse(savedLocations));
      else
        setLocationList([])
      await setLatitude(""+location.latitude)
      await setLongitude(""+location.longitude)
      await setLocName("GPS")
    })();
  }, []);

  return (
      <View style={styles.container}>
        <StatusBar style="auto" />

        <View style={styles.changeForm}>

          <TouchableOpacity style={styles.changePwdBtn} 
              onPress={() => {navigation.navigate('Change Password', { JWT: JWT, profile: profile, location:location});} }>
            <Text style={styles.changePwdText}>Change Password</Text>
          </TouchableOpacity>

          <View style={styles.locationBtns}>

          <TouchableOpacity style={styles.locationBtn}
              onPress={async() => { await setGPSLocation() }}>
            <Text style={styles.locationText}>Set GPS Location</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.locationBtn}
              onPress={async() => { await setLocation() }}>
            <Text style={styles.locationText}>Set Location</Text>
          </TouchableOpacity>
        
        </View>

        <View style={styles.latLong}>
          <TextInput
            style={styles.latLongInput}
            value={locName}
            placeholder="Name"
            placeholderTextColor="#fff"
            onChangeText={(locName) => setLocName(locName)}
          />

          <TextInput
            style={styles.latLongInput}
            value={latitude}
            placeholder="Latitude"
            placeholderTextColor="#fff"
            onChangeText={(latitude)=>setLatitude(latitude)}
          />

          <TextInput
            style={styles.latLongInput}
            value={longitude}
            placeholder="Longitude"
            placeholderTextColor="#fff"
            onChangeText={(longitude)=>setLongitude(longitude)}
          />

          <TouchableOpacity style={styles.addLocBtn}
            onPress={async() => { await saveLoc() }}>
            <Text style={styles.addLocText}>💾</Text>
          </TouchableOpacity>
        </View>

        <LocListComponent data={locationList}/>

      </View >
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height:"100%",
    backgroundColor: "#040500",
    paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
  },

  changeForm:{
    height:"100%",
  },

  inputView: {
    backgroundColor: "#d93128",
    borderRadius: 30,
    width: "90%",
    height: 30,
    marginBottom:10,
    alignSelf: "center",
  },

  TextInput: {
    textAlign:"center",
    fontSize: 20,
    color: "#fff"
  },

  changePwdBtn: {
    width: "90%",
    borderRadius: 25,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#ebedef",
  },

  changePwdText: {
    color: "#303841",
    fontWeight: "bold",
    fontSize: 20
  },

  locationBtns:{
    alignSelf:"center",
    alignItems:"center",
    flexDirection: 'row',
    margin:'1%'
  },

  locationBtn: {
    width: "47%",
    height: 30,
    margin: "1%",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#303841",
  },

  locationText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 20,
  },


  latLong: {
    alignSelf:"center",
    display: 'flex',
    flexDirection: 'row',
  },

  latLongInput: {
    flex: 1,
    borderRadius: 5,
    borderWidth:0.1,
    borderColor:"#fff",
    backgroundColor: "#d93128",
    width:"100%",
    textAlign:"center",
    fontSize: 20,
    color:"#fff",
    margin:"1%",
  },

  latLongItem: {
    borderColor:"#fff",
    borderWidth:0.5,
    borderRadius: 5,
    backgroundColor: "#303841",
    alignItems:"center",
    justifyContent:"center",
    alignSelf:"center",
    width:"90%",
    flexDirection: 'row',
    marginTop:10
  },

  selectLocBtn: {
    width:"100%",
    margin:"1%",
    alignItems: "center",
  },

  selectLocText: {
    color: "#fff",
    fontSize: 20,
  },

  deleteLocBtn: {
    margin:"1%",
    alignItems: "center",
  },

  deleteLocText: {
    color: "#055",
    fontSize: 25,
  },

  addLocBtn: {
    margin:"1%",
    alignItems: "center",
  },

  addLocText: {
    color: "#fff",
    fontSize: 20,
  },


});

export default SettingsScreen;