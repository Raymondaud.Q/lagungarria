import { NavigationContainer } from '@react-navigation/native';
import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import APIServices from './APIServices.js';
import Utils from './utils.js';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert
} from "react-native";

const PasswordForgottenScreen = ({ navigation }) => {
  const [email, setEmail] = useState("");
  
  return (
    <View style={styles.container}>

      <StatusBar style="auto" />

      <Image
        style={styles.image}
        source={require('../assets/logo.jpeg')}
      />

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email"
          placeholderTextColor="#fff"
          onChangeText={(email) => setEmail(email)}
        />
      </View>

      <TouchableOpacity style={styles.passwordResetBtn}
        onPress={() => {
            APIServices.resetPassword(email).then((json) => {
              if ( json.status != 201 ){
                Utils.showAlert("Reset Email Request FAILED", JSON.stringify(json) + "--" + JSON.stringify(json.text()));
                return false;
              } else {
                showAlert("Reset Email has been sent", "Check your inbox !");
                return json.text();
              }
            }).then((res) => {
              if ( res )
                navigation.navigate('Login', { access: JSON.parse(res).access_token  } );
            });
          }
        }>
        <Text style={styles.passwordResetText}>Reset Password</Text>
      </TouchableOpacity>
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#040500",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    width: "50%",
    height: "20%",
    marginBottom: 100,
  },

  inputView: {
    backgroundColor: "#d93128",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    width:"100%",
    textAlign:"center",
    fontSize: 20,
    color: "#fff"
  },


  passwordResetBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#ebedef",
  },

  passwordResetText: {
    color: "#303841",
    fontWeight: "bold",
    fontSize: 20,
  },
});

export default PasswordForgottenScreen;