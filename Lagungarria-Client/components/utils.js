import * as SecureStore from 'expo-secure-store';
import {  Alert } from "react-native";
import {getDistance, getPreciseDistance} from 'geolib';

const showAlert = (title,msg) => {
  return Alert.alert(
    title,
    msg,
    [{
  	  text: "Ok",
  	  //onPress: () => Alert.alert("Ok Pressed"),
  	  style: "Ok",
    }],
    {
      cancelable: true,
      // onDismiss: () => Alert.alert("This alert was dismissed by tapping outside of the alert dialog."),
    }
  );
}

async function save(key, value) {
  try {
    await SecureStore.setItemAsync(key, value);
  } catch (error) {
    console.log("SecureStore save error !", error);
  }
}

const calculateDistance = (src, dest) => {
  if ( dest == undefined || src == undefined)
    return "Unknown Location";
  var pdis = getPreciseDistance(src,dest);
  return pdis/1000; // KM
};

export default {showAlert, calculateDistance, save};