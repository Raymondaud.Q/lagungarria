import { NavigationContainer, useFocusEffect } from '@react-navigation/native';
import React, { useState, useEffect } from "react";
import { StatusBar } from "expo-status-bar";
import Swiper from 'react-native-swiper'
import * as Location from 'expo-location';
import Carousel from 'react-native-snap-carousel';
import { myAddress } from '../HttpConfig.js';
import * as SecureStore from 'expo-secure-store';
import APIServices from './APIServices.js';
import * as ImagePicker from 'expo-image-picker';
import Lightbox from 'react-native-lightbox';
import { format } from "date-fns";
import Utils from './utils.js';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Icon,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  Dimensions,
  Platform,
  ScrollView,
  Clipboard
} from "react-native";
import { LogBox } from 'react-native'; // Ignores warnings flow from lightbox 

var _carousel;

const HomeScreen = ({ navigation, route }) => {

  useEffect(() => { // Ignores Warnings from LightBox default params
    LogBox.ignoreLogs(['Animated: `useNativeDriver`', 'Animated.event now requires a second argument for options']);
  }, [])

  const { JWT, profile } = route.params; //JWToken received;
  const [JWTaccess, setJWTaccess] = useState(null);
  const [user, setUser] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [currentLoc, setCurrentLoc] = useState(null);
  const [newPost, setNewPost] = useState(null);
  const [postList, setPostList] = useState(null);
  const perimeter = 200;
  const [dCarousel, setCarousel ] = useState(null);
  const [image, setImage] = useState(null);
  
  const chooseImg = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      //aspect: [4, 3],
      //quality: 1,     
      allowsEditing: true,
    });
    //console.log(result);
    if (result && result.uri) {
      setImage(result.uri);
    }
  };

  const locationRoutine = async() => {
    var tmpLocation = await SecureStore.getItemAsync('location')
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status === 'granted' && tmpLocation===''){
      try{
        let location = await Location.getCurrentPositionAsync( {enableHighAccuracy:true} );
        tmpLocation = {latitude: location.coords.latitude, longitude: location.coords.longitude}
        await setCurrentLoc( tmpLocation );
        return JSON.stringify(tmpLocation)
      } catch(e) {
        tmpLocation = {latitude:0, longitude:0}
        await setCurrentLoc(tmpLocation)
        let strMsg = "Actual location = Lat:"+tmpLocation.latitude+"/Long:"+tmpLocation.longitude;
        Utils.showAlert('Location disabled','Location Permission granted but location module disabled\n'+strMsg);
        return JSON.stringify(tmpLocation);
      }
    } else {
      if ( status !== 'granted' && tmpLocation === '' || !tmpLocation ){
        tmpLocation = JSON.stringify({latitude:0, longitude:0})
        let messageLoc = JSON.parse(tmpLocation)
        let strMsg = "Actual location = Lat:"+messageLoc.latitude+"/Long:"+messageLoc.longitude;
        Utils.showAlert('Device Location Permission',' No permission to access device location granted\n'+strMsg);
      }
      await setCurrentLoc(JSON.parse(tmpLocation))
    }
    return tmpLocation
  }

  useFocusEffect( // On focus update location
    React.useCallback(() => {
      (async () => {
        let tmpLocation = await locationRoutine();
        if (Platform.OS !== 'web') {
          const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
          if (status !== 'granted')
            Utils.showAlert('Permission denied','Image Roll permissions are refused but they are required to send pictures');
        }
        await setJWTaccess(JWT);
        await setUser(JSON.parse(profile))
        updatePostList(JWT, JSON.parse(tmpLocation), perimeter);
      })();
    },[])
  );

  const refreshToken = async() => {
    let token =  await SecureStore.getItemAsync('token');
    let user =  await SecureStore.getItemAsync('profile');
    let password =  await SecureStore.getItemAsync('password');
    APIServices.testToken( token, JSON.parse(user), password).then((json) => {
      if ( json.status != 200 ){
        APIServices.login(JSON.parse(user).username, password).then((json) => {
          if ( json.status != 200 ){
            Utils.showAlert("Refresh Token FAILED", "Wrong credentials loaded !\n Please Log-in Manually ")
            navigation.navigate('Login', {} );
            return false;
          } else 
            return json.text();
        }).then((res) => {
          if ( res ){
            setJWTaccess(JSON.parse(res).access_token)
            setUser(JSON.parse(res).user)
            Utils.save('token', JSON.parse(res).access_token);
          }
        })
      }
    })
  }

  const updatePostList = (token, location, perimeter) => {
    APIServices.updatePostList(token,location,perimeter).then((json) => {
      if ( json.status != 200 ){
        refreshToken();
        return false;
      } else {
        return json.text();
      }
    }).then((res) => {
      if ( res ){
        setPostList(JSON.parse(res));
      }
    });
  }

  const post = (token,nick,text,location,image) => { 
    APIServices.post(token,nick,text,location,image).then((response) => {
      return response.text()
    }).then((data) => {
      data = JSON.parse(data)
      if ( data.detail ){
        Utils.showAlert( "ERROR - POST Failed", data.messages[0].message+ "\n\nToken refreshed, try again" )
        refreshToken()
      } else if ( data.textContent && ! data.id){
        Utils.showAlert( "ERROR - POST Failed ", "TextContent Error : " + data.textContent[0] )
      } else {
        updatePostList(token, currentLoc, perimeter);
        setNewPost("")
        setImage()
      }
    })
  }

  const remove = (token,id) => { 
    APIServices.remove(token, id).then((json) => {
      if ( json.status != 204 ){
        Utils.showAlert("Error REMOVE "+json.status , "Your post HASN'T been removed")
        refreshToken();
        return false;
      } else {
        updatePostList(token, currentLoc, perimeter);
      }
    })
  }


  const DistLocComponent = ( item) => {
    let dist = Utils.calculateDistance(currentLoc, item)
    let latLongStr =item.latitude+","+item.longitude
    return(
      <TouchableOpacity  style={styles.postLocBtn}
          onPress={() => { 
            Clipboard.setString(latLongStr); 
            Utils.showAlert("Position Copied to Cliboard",item.nickname+"'s post Lat/Long :\n"+latLongStr)
          }}>
          <Text style={styles.postLocText}> {item.nickname == user.username ? "YOU" : item.nickname }, {JSON.stringify(dist)} Km</Text>
      </TouchableOpacity>
    );
  }

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (currentLoc) {
    text = JSON.stringify(currentLoc);
  }

  const renderItem = ({ item, index }) => {
    return(
      <View style={styles.itemContainer}>
          
        { item.nickname == user.username && <TouchableOpacity style={styles.removeBtn}
          onPress={() => remove( JWTaccess, item.id)}>
          <Text style={styles.removeText}>❌</Text>
        </TouchableOpacity>}
        { item.image != null && <Lightbox style={{flex:1}}>
          <Image
            style={styles.itemImg}
            source={{ uri: "http://"+myAddress()+item.image }}
          />
        </Lightbox> }
        <View style={styles.itemPost}>
          <Text style={styles.textItem1}> { format(new Date(item.date), "dd-MM-yyyy HH:mm:ss") } </Text>
          {DistLocComponent(item)}
          { item.textContent!="null" && <Text style={styles.textItem2}> {item.textContent} </Text> }
        </View>
      </View>);
  } 

  return (
    <View style={styles.wrapper}>

      {postList != null && postList.length != 0 && <Carousel
        ref={(carousel) => { setCarousel(carousel) }}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
        activeSlideOffset={100}
        inactiveSlideOpacity={0}
        inactiveSlideScale={0.5}
        renderItem={renderItem}
        data={postList}
        onSnapToItem = {(slideIndex) => { updatePostList(JWTaccess, currentLoc, perimeter);}}>
      </Carousel>}

      { (postList == null || postList.length == 0 ) && <Text style={styles.emptyState}>{"Empty State"}</Text>}

      <View style={styles.formItem}>
        <TouchableOpacity style={styles.settingsBtn}
          onPress={() => {navigation.navigate('Settings', { JWT: JWTaccess, profile: JSON.stringify(user), location:currentLoc });} }>
          <Text style={styles.settingsText}>⚙️</Text>
        </TouchableOpacity>

        <TextInput
            multiline={true}
            value={newPost}
            style={styles.TextInput}
            placeholder="Here you can write your message"
            placeholderTextColor="#fff"
            onChangeText={newPost => setNewPost(newPost)}
          />

        <TouchableOpacity style={styles.publishBtn}
          onPress={() => { post(JWTaccess, user.username, newPost, currentLoc, image) }}>
          <Text style={styles.publishText}>💬</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.moreBtn}
          onPress={() => {chooseImg()} }>
          <Text style={styles.moreText}>📷</Text>
        </TouchableOpacity>
      </View>

      {image && <View style={styles.formItem}>
        <TouchableOpacity style={styles.delImgBtn}
          onPress={() => {setImage()} }>
          <Text style={styles.moreText}>❌</Text>
        </TouchableOpacity>
        <Image source={{ uri: image }} style={{margin:20, alignSelf:"center", width: 200, height: 200 }} />
      </View>}
      
    </View>
  );
}

const styles = StyleSheet.create({

  itemImg: {
    resizeMode: "center",
    height:"100%",
    width:"100%",
  },
  wrapper: {
    height:"100%",
    width:"100%",
    flexDirection: 'column',
    backgroundColor: "#222",
  },

  itemContainer: {
    flex: 1,
    alignSelf: 'stretch',
    padding: 20,
    marginTop:40,
    margin:20,
    backgroundColor: '#303841',
    borderColor:"#fff",
    borderRadius: 30,
    borderWidth:1
  },

  itemPost: {
    alignItems: "center",
  },

  textItem1: {
    color: "#fff",
    fontSize: 15,
  },

  textItem2: {
    color: "#fff",
    paddingTop:"2%",
    fontSize: 15,
  },

  viewContainer: {
    paddingTop: 0,
    height: "100%",
    backgroundColor: "#222",
  },

  formItem: {
    alignItems:"center",
    alignSelf:"center",
    display: 'flex',
    flexDirection: 'row',
    paddingBottom:"1%",
    paddingLeft:"5%",
    paddingRight:"5%",
    backgroundColor:"#222",
  },

  publishBtn: {
    alignItems: "center",
    width: "10%",
  },

  postLocBtn: {
    alignSelf:"center",
    backgroundColor: "#fff",
    borderRadius: 6,
  },

  postLocText: {
    color: "#000",
    fontWeight: "bold",
    fontSize: 15,
    padding:5
  },

  moreBtn: {
    alignItems: "center",
    width: "10%",
  },

  delImgBtn: {
    width: "10%",
  },

  settingsBtn: {
    alignItems: "center",
    width: "10%",
    padding:"1%"
  },

  publishText: {
    color: "#fff",
    fontSize: 20,
  },

  moreText: {
    color: "#fff",
    fontSize: 20,
  },

  delImgText: {
    color: "#fff",
    fontSize: 20,
  },

  settingsText: {
    color: "#fff",
    fontSize: 20,
    width: "100%",
  },

  emptyState: {
    color: "#fff",
    fontSize: 50,
    alignSelf: "center",
    top:"40%",
    flex: 1,
  },

  removeBtn: {
    alignItems:"center",
    width:"10%",
    borderRadius: 40,
    backgroundColor: "#303841",
  },

  removeText: {
    color: "#fff",
    fontSize: 20,
  },

  inputView: {
    backgroundColor: "#d93128",
    width: "80%",
    alignItems: "center",
  },

  TextInput: {
    padding: "2%",
    width:"80%",
    display:"flex",
    borderRadius: 30,
    fontSize: 15,
    backgroundColor: "#d93128",
    color: "#fff"
  }

});

export default HomeScreen;
