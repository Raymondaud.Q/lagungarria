import { NavigationContainer } from '@react-navigation/native';
import React, { useState, useEffect  } from "react";
import { StatusBar } from "expo-status-bar";
import * as SecureStore from 'expo-secure-store';
import APIServices from './APIServices.js';
import Utils from './utils.js';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert
} from "react-native";

const login = (navigation, username, password) => {
  APIServices.login(username,password).then((response) => {
    return ( response.status == 200 ? response.json() : false );
  }).then((data) => {
    if ( data ){
      let JWTaccess= data.access_token
      let user = data.user
      Utils.save('token', JWTaccess );
      Utils.save('profile', JSON.stringify(user));
      Utils.save('password', password);
      navigation.navigate('Home', { JWT: JWTaccess, profile: JSON.stringify(user)  } );
    } else 
      Utils.showAlert("ERROR - Authentication Failed", "Wrong username or password !")
  })
}

const LoginScreen = ({ navigation }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  useEffect(() => {
    (async () => {
      let token =  await SecureStore.getItemAsync('token');
      let user =  await SecureStore.getItemAsync('profile');
      let password =  await SecureStore.getItemAsync('password');
      if ( token && user && password )
        APIServices.testToken(token, JSON.parse(user), password).then((json) => {
          if ( json.status == 200 )
            navigation.navigate('Home', { JWT: token, profile: user  } );
          else if ( JSON.parse(user).username && password )
            login(navigation, JSON.parse(user).username, password)
        })
    })();
  },[]);

  return (
    <View style={styles.container}>

      <StatusBar style="auto" />

      <Image
        style={styles.image}
        source={require('../assets/logo.jpeg')}
      />

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Username"
          placeholderTextColor="#fff"
          onChangeText={(username) => setUsername(username)}
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          placeholderTextColor="#fff"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>

      <TouchableOpacity
        onPress={() => navigation.navigate('Reset', {}) }>
        <Text style={styles.forgotBtn}>Forgot Password?</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.loginBtn}
          onPress={() => { login(navigation, username, password)}}>
        <Text style={styles.loginText}>Login</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.registerBtn} 
          onPress={() => navigation.navigate('Register', {}) }>
        <Text style={styles.registerText}>Create an account</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#040500",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    width: "50%",
    height: "20%",
    marginBottom: 100,
  },

  inputView: {
    backgroundColor: "#d93128",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    width:"100%",
    textAlign:"center",
    fontSize: 20,
    color: "#fff"
  },

  forgotBtn: {
    height: 30,
    marginBottom: 30,
    color: "#fff"
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#303841",
  },

  loginText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 20,
  },

  registerBtn: {
    width: "60%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#ebedef",
  },

  registerText: {
    color: "#303841",
    fontWeight: "bold",
    fontSize: 20,
  },
});

export default LoginScreen;