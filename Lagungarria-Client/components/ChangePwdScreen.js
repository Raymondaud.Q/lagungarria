import { NavigationContainer } from '@react-navigation/native';
import React, { useState, useEffect } from "react";
import BigList from "react-native-big-list";
import * as Location from 'expo-location';
import { StatusBar } from "expo-status-bar";
import { myAddress } from '../HttpConfig.js';
import APIServices from './APIServices.js';
import * as SecureStore from 'expo-secure-store';
import Utils from './utils.js';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  ScrollView,
  Dimensions
} from "react-native";


const SettingsScreen = ({ navigation, route }) => {
  const { JWT, profile, location, HomeComponent } = route.params;
  const [password, setPassword] = useState("");
  const [pwdV, setPwdV] = useState("");
  const [show, setShow] = useState(true)

  const changePwd = () => {
    if ( pwdV === password ){
      APIServices.changePassword(JWT, password).then((response) => {
        return response.text()
      }).then((data) => {
        data = JSON.parse(data)
        if ( data.detail == "New password has been saved."){
          Utils.showAlert( "Password Changed", "Your new password has been saved !" )
          navigation.navigate('Login', {});
        } else if (data.messages){
          refreshToken()
        } 

        if (data){
          let strError = ""
          if ( data.new_password1 )
            strError += "Password1 : " +data.new_password1+"\n"
          if ( data.new_password2 )
            strError += "Password2 : " +data.new_password2+"\n"
          if (strError !== "")
            Utils.showAlert( "ERROR - ChangePwd Failed ", strError )
        } 
      })
    } else {
      Utils.showAlert( "ERROR - ChangePwd Failed ", "Password fields are differents,\nPlease provide the same password twice !" )
    }
  }

  return (
    <View style={styles.container}>

      <StatusBar style="auto" />

        <Image
          style={styles.image}
          source={require('../assets/logo.jpeg')}
        />
        
      <View style={styles.changeForm}>

        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="New Password"
            placeholderTextColor="#fff"
            secureTextEntry={show}
            onChangeText={(password) => setPassword(password)}
          />
        </View>

        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Again New Password "
            placeholderTextColor="#fff"
            secureTextEntry={show}
            onChangeText={(pwdV) => setPwdV(pwdV)}
          />
        </View>

        <TouchableOpacity style={styles.showPwdBtn} 
            onPress={() => {setShow(!show)} }>
          <Text style={styles.showPwdText}>Show 👀</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.changePwdBtn} 
            onPress={() => {changePwd()} }>
          <Text style={styles.changePwdText}>Change Password !</Text>
        </TouchableOpacity>

      </View >
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height:"100%",
    backgroundColor: "#040500",
    paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
  },

  image: {
    marginTop: 20,
    width: "50%",
    height: "20%",
    alignSelf: "center"
  },

  changeForm:{
    height:"100%",
    marginTop: 10,
  },

  inputView: {
    backgroundColor: "#d93128",
    borderRadius: 30,
    width: "90%",
    height: 30,
    marginTop:10,
    alignSelf: "center",
  },

  TextInput: {
    textAlign:"center",
    fontSize: 20,
    color: "#fff"
  },

  changePwdBtn: {
    width: "90%",
    borderRadius: 25,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#ebedef",
  },

  changePwdText: {
    color: "#303841",
    fontWeight: "bold",
    fontSize: 20,
    margin: 5,
  },

  showPwdBtn: {
    width: "90%",
    borderRadius: 25,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#555",
  },

  showPwdText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 20,
    margin: 5,
  },

});

export default SettingsScreen;