import { myAddress } from '../HttpConfig.js';


const showAlert = (title,msg) =>
  Alert.alert(
    title,
    msg,
    [
      {
        text: "Ok",
        //onPress: () => Alert.alert("Ok Pressed"),
        style: "Ok",
      },
    ],
    {
      cancelable: true,
      // onDismiss: () =>
        // Alert.alert(
           //"This alert was dismissed by tapping outside of the alert dialog."
        //),
    }
  );


const testToken = ( token, user, password ) => {
  return fetch("http://"+myAddress()+"/dj-rest-auth/token/verify/", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      token: token
    })
  })
}

const login = ( username, password) => {
  return fetch('http://'+myAddress()+'/dj-rest-auth/login/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username: username,
      password: password
    })
  });
}

export default { testToken, login };