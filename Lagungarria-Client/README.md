# Installation 

* sudo apt install npm
* sudo npm -g install npm@8.3.1
* sudo npm -g install node@16.13.1
* sudo npm install -g expo-cli@5.0.3

# Run : Dev Env 

## Setup

* cd Lagungarria
* Install dependencies :  `npm i`
* npm run test
* Connect your device in dev mode thourgh USB
* Install **Expo Go** on your device : `expo client:install:android`
* (Optional) Build the project `expo build:android`
* Edit `HttpConfig.js` by replacing the current address by your server address
* Start the dev server : `expo start`
* Open **Expo Go** on your device
* Scan the **QRCODE** with Expo Go to open the app