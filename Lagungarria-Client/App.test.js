import { NavigationContainer, useFocusEffect } from '@react-navigation/native';
import React, { useState, useEffect  } from "react";
import renderer from 'react-test-renderer';
import  LoginScreen  from './components/LoginScreen.js';
import  HomeScreen  from './components/HomeScreen.js';
import  RegisterScreen  from './components/RegisterScreen.js';
import APIServices from './components/APIServices.js';
import "isomorphic-fetch";

jest.useFakeTimers()
jest.mock('expo-location', ()=>({}));
jest.mock('expo-image-picker', ()=>({}));
jest.mock("@react-navigation/native", () => {
  const actualNav = jest.requireActual("@react-navigation/native");
  return {
    ...actualNav,
    useNavigation: () => ({
      navigate: jest.fn(),
      dispatch: jest.fn(),
    }),

    useFocusEffect: () => ({

    })
  };
});
jest.mock("./HttpConfig.js", () => {
  return {
    __esModule: true,
    myAddress: () => {
      // Here you can edit docker to your backend's ip
      return "docker:8000";
    },
  };
});

const navigation = { navigate: jest.fn() };
let username='test-user'
let mail='test-email@mockmail.com'
let password='test-password1234';

describe('<Register />', () => {
  it('has 3 child', () => {
    const view = renderer.create(<RegisterScreen route={{}} navigation={{navigation}}/>)
    const tree = view.toJSON()
    const root = view.root
    expect(tree.children.length).toBe(5);
  });

  it('allows to register', async() => {
    const data = await APIServices.register(username,password,mail).then((response) => {
      return response
    }).catch(err => console.error(err));
    expect(data.status).toBe(201)
  })
})

describe('<LoginScreen />', () => {
  it('has 6 child', () => {
    const view = renderer.create(<LoginScreen />)
    const tree = view.toJSON()
    const root = view.root
    expect(tree.children.length).toBe(6);
  });

  it('allows to login', async() => {
    const data = await APIServices.login(username,password).then((response) => {
      return response
    }).catch(err => console.error(err));
    expect(data.status).toBe(200)
  })
})

describe('<HomeScreen />', () => {
  it('has 2 child', () => {
    const data ={params: {JWT: "fake", profile: "news"}}
    const view = renderer.create(<HomeScreen route={data} navigation={{navigation}}/>)
    const tree = view.toJSON()
    const root = view.root
    expect(tree.children.length).toBe(2);
  });

  it('allows to post snippets', async() => {
    const data = await APIServices.login(username,password).then((response) => {
      return response.json()
    }).then((data)=>{
      return {params: {JWT: data.access_token, profile: data.user}}
    })
    const msg = await APIServices.post(data.params.JWT, username, "Test Msg", {latitude:0.0,longitude:0.0} ).then((response) => {
      return response
    }).catch(err => console.error(err));
    expect(msg.status).toBe(201)
  })

  it('allows to read snippets', async() => {
    const data = await APIServices.login(username,password).then((response) => {
      return response.json()
    }).then((data)=>{
      return {params: {JWT: data.access_token, profile: data.user}}
    })
    const msg = await APIServices.updatePostList(data.params.JWT, {latitude:0.0,longitude:0.0}, 100 ).then((response) => {
      return response
    }).catch(err => console.error(err));
    expect(msg.status).toBe(200)
  })
})
